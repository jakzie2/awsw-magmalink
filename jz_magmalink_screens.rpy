screen _ml_mod_settings tag smallscreen2:
    modal True

    window id "_ml_mod_settings" at popup2:
        style "smallwindow"

        add "image/ui/settings_title.png" xalign 0.01 yalign 0.1 at title_slide
        
        if _ml_screen_nav:
            imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("_ml_mod_settings"), Show("preferences_nav"), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" at nav_button
        else:
            imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("_ml_mod_settings"), Show("preferences"), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" at nav_button

screen _ml_status_cards tag smallscreen2:
    modal True
    
    window id "_ml_status_cards" at popup2:
        style "smallwindow"
        
        hbox:
            xalign 0.5 yalign 0.5
            spacing 30
        
        imagebutton idle "image/ui/close_idle.png" hover "image/ui/close_hover.png" action [Hide("_ml_status_cards"), Play("audio", "se/sounds/close.ogg")] hovered Play("audio", "se/sounds/select.ogg") style "smallwindowclose" at nav_button

screen _ml_qte_screen(qte_action, qte_time):
    timer qte_time action Return(False)
    modal True
    window id "_ml_qte":
        textbutton qte_action action Return(True) at top
