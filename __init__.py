import sys
import importlib
from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod
from renpy.exports import error

@loadable_mod
class AWSWMod(Mod):
    name = "MagmaLink"
    version = "0.2.2"
    author = "Jakzie"
    
    def mod_load(self):
        if not any([pth.endswith("modules") for pth in sys.path]):
            error("Old version of the modloader detected.\nPlease make sure you have the newest version of Modtools installed either from Workshop or GitHub.")
        
        modast.set_renpy_global("magmalink", self.import_ml)
        
        import jz_magmalink.common as common
        common._loading = False
    
    def mod_complete(self):
        pass
    
    def import_ml(self, module=None):
        return importlib.import_module("jz_magmalink" + ("" if module is None else "." + module))
