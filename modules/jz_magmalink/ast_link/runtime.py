import renpy
from renpy import ast, python

import jz_magmalink.ast_link.utils as utils

class Store:
    def __init__(self, name):
        self.name = name
    
    def __getitem__(self, key):
        return getattr(*self._parse_key(key))
    
    def __setitem__(self, key, value):
        p, k = self._parse_key(key)
        setattr(p, k, value)
    
    def __delitem__(self, key):
        delattr(*self._parse_key(key))
    
    def __contains__(self, key):
        return hasattr(*self._parse_key(key))
    
    def _parse_key(self, key):
        parts = key.split(".")
        p = renpy.python.store_modules[self.name]
        for i in range(len(parts) - 1):
            p = getattr(p, parts[i])
        return p, parts[-1]
    
    def export(self, name):
        def func(fcn):
            self.__setitem__(name, fcn)
            return fcn
        return func
    
    def create_store(self, name):
        renpy.python.create_store(self.name + "." + name)
        
        return Store(self.name + "." + name)
    
    def get_store(self, name):
        if self.name + "." + name in renpy.python.store_dicts:
            return Store(self.name + "." + name)
        return None

class Context(Store):
    def __init__(self):
        self.name = "store"
    
    def get_current(self):
        return utils.node(renpy.game.script.lookup(renpy.game.context().current))
    
    def set_next(self, call=None, jump=None):
        if jump is not None:
            ast.next_node(utils._parse_jump(jump))
        if call is not None:
            ast.next_node(renpy.game.context().call(utils._parse_jump(call).name, return_site=renpy.game.context().next_node.name))

class Eval:
    def __getitem__(self, key):
        return python.py_eval(key)
    
    def __call__(self, arg):
        return python.py_eval(arg)

_running = False
context = Context()

def _init():
    global _running
    _running = True
    context["ml_eval"] = Eval()
