from functools import partial
from renpy import ast, python
from renpy.game import script
from modloader import modast

import jz_magmalink.common as common
import jz_magmalink.ast_link.utils as utils

_messages = []

def add_message(label, condition=None, chapters=[1,2,3,4]):
    global _messages
    if not (isinstance(label,(str,unicode)) and script.has_label(label)):
        common.error("The first argument to add_message must be a label.")
    _messages.append((label, condition, chapters))

def _check_message_conditions(conditions,hook):
    ast.next_node(hook.next)
    if any(python.py_eval(condition) for condition in conditions):
        modast.set_renpy_global('playmessage',True)

def _play_message(label,condition,hook):
    ast.next_node(hook.old_next)
    if python.py_eval(condition):
        ast.next_node(hook.next)
        modast.set_renpy_global('popularnumber',modast.get_renpy_global('popularnumber')+1)
        modast.set_renpy_global(label+'_unheard',False)
    return True

def _get_nextif_with_cond(cond):
    def closure(n):
        if not isinstance(utils._next(n),ast.If):
            return False
        for (_cond,_) in utils._next(n).entries:
            if cond == _cond:
                return True
        return False
    return closure


def _build_messages():
    c1hookpoint = utils.find_label('chapter1chars') \
        ._search(_get_nextif_with_cond('sebastianunplayed == False'), 200, "'sebastianunplayed == False' not found under chapter2chars label.") \
        .hook_to('_ml_core_chapter1_messagesalready',condition='playmessage == True')

    hookpoints = [
        (1,c1hookpoint),
        (2,utils.find_label('chapter2chars')._search(_get_nextif_with_cond('playmessage == True'), 200, "'playmessage == True' not found under chapter2chars label.")),
        (3,utils.find_label('chapter3chars')._search(_get_nextif_with_cond('playmessage == True'), 200, "'playmessage == True' not found under chapter3chars label.")),
        (4,utils.find_label('chapter4chars')._search(_get_nextif_with_cond('playmessage == True'), 200, "'playmessage == True' not found under chapter4chars label.")),
    ]

    for chapter,hookpoint in hookpoints:
        messages = [m for m in _messages if chapter in m[2]]
        if not messages:
            continue
        conditions = [(label+'_unheard'+(' and ('+condition+')' if condition else '')) for label,condition,_ in messages]

        utils._create_hook(node_from=hookpoint.node,func=partial(_check_message_conditions,conditions))

        if chapter==1:
            hookpoint = utils.find_label('_ml_core_chapter1_messagesalready')\
                .search_say("(Looks like there are already some messages on the answering machine. Let's see...)")
        else:
            # If it's not the next node, something is wrong with the AST 'cause it was when the loop started.
            hookpoint = hookpoint.search_if('playmessage == True',depth=2)

        for condition,(label,_,_) in zip(conditions,messages):
            modast.set_renpy_global(label+'_unheard',True)

            h = utils._create_hook(
                node_from=hookpoint.node,
                node_to=modast.find_label(label),
                func=partial(_play_message,label,condition))

            
            hookpoint.node.chain(h)

        # Hook this last so it will run after each message, when the messages return to the below label.
        hookpoint.hook_call_to('popularcheck')

        # Finally, create the return label last to return to the beginning of the answering machine message stack.
        utils._create_label('_ml_core_chapter%u_answeringmachinehookpoint'%chapter, hookpoint.node.next)
