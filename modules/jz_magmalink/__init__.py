import jz_magmalink.ast_link.utils as ast_utils
import jz_magmalink.ast_link.base as ast_base
import jz_magmalink.ast_link.nodes as ast_nodes
import jz_magmalink.ast_link.runtime as runtime
import jz_magmalink.ast_link.builders.route as route
import jz_magmalink.ast_link.builders.answeringmachine as answeringmachine

import jz_magmalink.sl_link.utils as sl_utils
import jz_magmalink.sl_link.base as sl_base
import jz_magmalink.sl_link.builders.overlay as overlay
import jz_magmalink.sl_link.builders.status as status
import jz_magmalink.sl_link.builders.achievements as achievements

import jz_magmalink.extras.menu as menu
import jz_magmalink.extras.pages as pages
import jz_magmalink.extras.game as game

node = ast_utils.node
find_label = ast_utils.find_label
find_character_menus = ast_utils.find_character_menus
find_ending_menu = ast_utils.find_ending_menu

Searchable = ast_base.Searchable
Hookable = ast_base.Hookable
Node = ast_base.Node
Branch = ast_base.Branch

OtherNode = ast_nodes.OtherNode
EndNode = ast_nodes.EndNode
MenuNode = ast_nodes.MenuNode
IfNode = ast_nodes.IfNode

context = runtime.context

CharacterRoute = route.CharacterRoute
add_answ_machine_message = answeringmachine.add_message

slnode = sl_utils.slnode
find_screen = sl_utils.find_screen
create_screen = sl_utils.create_screen

SLNode = sl_base.SLNode
SLBlock = sl_base.SLBlock
SLBranch = sl_base.SLBranch

Overlay = overlay.Overlay

StatusBox = status.StatusBox
add_status_item = status.add_status_item
add_status_card = status.add_status_card

add_achievement = achievements.add_achievement

register_mod_settings = menu.register_mod_settings

Game = game.Game
init_game = game.init_game
play_game = game.play_game
bind_game_store = game.bind_game_store

def _core_init():
    pages._link_charmenuframework()
    answeringmachine._build_messages()
    menu._create_mod_menu()
    status._create_status_screen()
    achievements._create_achievements_screen()
    runtime._init()
