from renpy.display import layout
from modloader import modinfo

import jz_magmalink.sl_link.utils as utils
import jz_magmalink.sl_link.base as base
import jz_magmalink.sl_link.builders.overlay as overlay

_LINE_LIMIT = 8
_mod_status_boxes = []
_mod_status_items = []
_mod_status_cards = []

class StatusBox:
    def __init__(self, scenes_played_var, condition="True"):
        self.status_list = []
        self.scenes_played_var = scenes_played_var
        self.condition = condition
    
    def add_status(self, image_path, status, condition):
        self.status_list.append((image_path, status, condition))
        
        return self
    
    def build(self):
        status_overlay = overlay.Overlay().add(["vbox:", "    if " + self.condition + ":"])
        for i, status in enumerate(self.status_list):
            if_st = "if" if i == 0 else "elif"
            status_overlay.add([
                "        " + if_st + " " + status[2] + ":",
                "            add \"" + status[0] + "\" xalign 0.5 at popup_offcenter",
                "            text _(\"    Status: {b}" + status[1] + "{/b}\\n    Scenes played: {b}[" + self.scenes_played_var + "]{/b}\") style \"status_text\" at popup_offcenter"
            ])
        _mod_status_boxes.append(status_overlay.build().first())

def add_status_item(image_path, condition):
    status_item = overlay.Overlay().add([
            "if " + condition + ":",
            "    add \"" + image_path + "\""
        ]).build()
    _mod_status_items.append(status_item.first())

def add_status_card(image_path, condition):
    status_card = overlay.Overlay().add([
            "if " + condition + ":",
            "    add \"" + image_path + "\""
        ]).build()
    _mod_status_cards.append(status_card.first())

def _create_status_screen():
    if len(_mod_status_boxes) == 0 and len(_mod_status_items) == 0 and len(_mod_status_cards) == 0:
        return
    
    old_screen = utils.find_screen("status")
    
    w1 = old_screen.search_window()
    
    if modinfo.has_mod("Sebastian's Ending"):
        b1 = w1.branch() \
            .search_box().branch() \
            .search_viewport()
        status_boxes = b1.branch() \
            .search_box().branch() \
            .collect(lambda n: n.search_box())
    else:
        b1 = w1.branch() \
            .search_box().branch() \
            .search_box()
        status_boxes = b1.branch() \
            .collect(lambda n: n.search_box())
        
    card_ifs = b1.search_box().branch() \
        .collect(lambda n: n.search_if())
    item_ifs = w1.search_box().branch() \
        .collect(lambda n: n.search_if())
    
    status_boxes.extend(_mod_status_boxes)
    item_ifs.extend(_mod_status_items)
    card_ifs.extend(_mod_status_cards)
    
    screen_overlay = overlay.Overlay() \
        .add([
            "window id \"save\" at popup_offcenter:",
            "    style \"smallwindow_big2\"",
            "    add \"image/ui/navmenu/status3.png\" yalign 0.1 at title_slide",
            "    vbox:",
            "        xalign 0.5 yalign 0.5",
            "        spacing 30"
        ])
    
    index = 0
    status_boxes_by_line = []
    for status_box in status_boxes:
        if index % _LINE_LIMIT == 0:
            status_boxes_by_line.append([])
            screen_overlay.add([
                "        hbox:",
                "            xalign 0.5 yalign 0.5",
                "            spacing 30"
            ])
        status_boxes_by_line[index // _LINE_LIMIT].append(status_box)
        index += 1
    
    screen_overlay.add([
        "hbox:",
        "    xalign 0.05 yalign 0.95",
        "    spacing 20",
        "hbox:",
        "    xalign 0.97 yalign 0.97",
        "    spacing 20",
        "    textbutton _(\"CARDS\") action [Show('_ml_status_cards'), Play(\"audio\", \"se/sounds/open.ogg\")] hovered Play(\"audio\", \"se/sounds/select.ogg\") style \"menubutton\""
    ])
    
    screen = screen_overlay.build()
    
    w2 = screen.search_window()
    w2.branch() \
        .search_box().branch() \
        .for_each(lambda n: n.search_box(), lambda i, n: n.branch().extend(status_boxes_by_line[i]))
    w2.search_box().branch() \
        .extend(item_ifs)
    
    old_screen.clear().extend(screen)
    
    utils.find_screen("_ml_status_cards") \
        .search_window().branch() \
        .search_box().branch() \
        .extend(card_ifs)
