from modloader import modast
from renpy import ast, parser

import jz_magmalink.sl_link.base as base

class Overlay:
    def __init__(self):
        self._tocompile = "screen dummy:\n    "
        self._default_xalign = 0
        self._default_yalign = 0
    
    def _parse_align(self, xalign, yalign):
        xalign = self._default_xalign if xalign is None else xalign
        yalign = self._default_yalign if yalign is None else yalign
        return xalign, yalign
    
    def with_align(self, xalign=None, yalign=None):
        self._default_xalign = self._default_xalign if xalign is None else xalign
        self._default_yalign = self._default_yalign if yalign is None else yalign
        return self
    
    def add_image(self, path, xalign=None, yalign=None, condition="True"):
        xalign, yalign = self._parse_align(xalign, yalign)
        return self.add("add \"" + path + "\" xalign " + str(xalign) + " yalign " + str(yalign), condition)
    
    def add(self, source_lines, condition=None):
        if isinstance(source_lines, (str, unicode)):
            source_lines = [source_lines]
        if condition is None:
            self._tocompile += "\n    ".join(source_lines) + "\n    "
        else:
            self._tocompile += "if " + condition + ":\n        " + "\n        ".join(source_lines) + "\n    "
        return self
    
    def build(self):
        compiled = parser.parse("FNDummy", self._tocompile)
        for node in compiled:
            if isinstance(node, ast.Init):
                return base.SLBranch(node.block[0].screen)
    
    def compile_to(self, screen_name):
        modast.get_slscreen(screen_name).children.extend(self.build().parent_node.children)
