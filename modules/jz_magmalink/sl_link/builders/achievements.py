import math
from renpy.sl2 import slast 

import jz_magmalink.sl_link.utils as utils
import jz_magmalink.sl_link.builders.overlay as overlay

_mod_achievements = []
_mod_achievement_conds = []

def add_achievement(name, description, image_path, condition, hidden=False):
    achievement = [
            "if " + condition + ":",
            "    add \"" + image_path + "\" xalign 0.5",
            "    text _(\"{b}" + name + " {i}(Mod){/i}{/b}\\n" + description + "\") style \"achievement_text\"",
            "else:",
            "    add \"image/ui/achievements/locked_achievement.png\" xalign 0.5",
            "    text _(\"" + ("{b}Hidden Achievement {i}(Mod){/i}{/b}" if hidden else ("{b}" + name + " {i}(Mod){/i}{/b}\\n" + description)) + "\") style \"achievement_text\""
    ]
    _mod_achievements.append(achievement)
    _mod_achievement_conds.append(condition)

def _create_achievements_screen():
    if len(_mod_achievements) == 0:
        return
    
    count_text = overlay.Overlay() \
        .add("text _(\"Mod achievement progress: [ml_eval[sum((" + ",".join(_mod_achievement_conds).replace("\"", "\\\"") + ",))]]/" + str(len(_mod_achievements)) + "\") xalign 0.92 yalign 0.9 size 22") \
        .build()
    
    utils.find_screen("achievements") \
        .search_window().branch() \
        .extend(count_text)
    
    w = utils.find_screen("achievement_page_5") \
        .search_window().branch()
    row1 = w.search_box()
    row2 = row1.search_box()
    row3 = row2.search_box()
    
    def search_box(n):
        if len(_mod_achievements) == 0:
            return None
        return n.search_box()
    
    def empty_replace(i, n):
        node = n.branch().first()
        if not isinstance(node.node, slast.SLIf):
            node.replace_by(overlay.Overlay().add(_mod_achievements.pop(0)).build().first())
    
    row1.branch().for_each(search_box, empty_replace)
    row2.branch().for_each(search_box, empty_replace)
    row3.branch().for_each(search_box, empty_replace)
    
    if len(_mod_achievements) == 0:
        return
    
    new_screen_count = int(math.ceil(len(_mod_achievements) / 15.0))
    
    new_button_next = overlay.Overlay() \
        .add("imagebutton idle \"image/ui/filepicker/Btn_Next.png\" hover \"image/ui/filepicker/Btn_Nextx.png\" action [Show('_ml_achievement_page_6'), Play(\"audio\", \"se/sounds/select3.ogg\")] xalign 0.88 yalign 0.55 at top_button hovered Play(\"audio\", \"se/sounds/select.ogg\")") \
        .build()
    new_button_prev = overlay.Overlay() \
        .add("imagebutton idle \"image/ui/filepicker/Btn_Prev.png\" hover \"image/ui/filepicker/Btn_Prevx.png\" action [Show('_ml_achievement_page_" + str(new_screen_count + 5) + "'), Play(\"audio\", \"se/sounds/select3.ogg\")] xalign 0.12 yalign 0.55 at top_button hovered Play(\"audio\", \"se/sounds/select.ogg\")") \
        .build()
    
    w.search_imagebutton() \
        .search_imagebutton() \
        .replace_by(new_button_next.first())
    utils.find_screen("achievement_page_1") \
        .search_window().branch() \
        .search_imagebutton() \
        .replace_by(new_button_prev.first())
    
    for i in range(new_screen_count):
        num = i + 6
        prev_page = "_ml_achievement_page_" + str(num - 1)
        next_page = "_ml_achievement_page_" + str(num + 1)
        if i == 0:
            prev_page = "achievement_page_5"
        if i == new_screen_count - 1:
            next_page = "achievement_page_1"
        
        new_screen_o = overlay.Overlay() \
            .add([
                "window at popup_offcenter:",
                "    background None",
                "    xpadding 0",
                "    ypadding 0",
                "    imagebutton idle \"image/ui/filepicker/Btn_Prev.png\" hover \"image/ui/filepicker/Btn_Prevx.png\" action [Show('" + prev_page +"'), Play(\"audio\", \"se/sounds/select3.ogg\")] xalign 0.12 yalign 0.55 at top_button hovered Play(\"audio\", \"se/sounds/select.ogg\")",
                "    imagebutton idle \"image/ui/filepicker/Btn_Next.png\" hover \"image/ui/filepicker/Btn_Nextx.png\" action [Show('" + next_page + "'), Play(\"audio\", \"se/sounds/select3.ogg\")] xalign 0.88 yalign 0.55 at top_button hovered Play(\"audio\", \"se/sounds/select.ogg\")"
        ])
        
        for _ in range(3):
            new_column_str = [
                "    vbox:",
                "        xalign 0.2",
                "        yalign 0.6",
                "        spacing 10"
            ]
            
            for _ in range(5):
                if len(_mod_achievements) == 0:
                    new_achiev_str = [
                        "        hbox:",
                        "            spacing 5",
                        "            add \"achievements/translucent.png\" xalign 0.5"
                    ]
                else:
                    new_achiev_str = [
                        "        hbox:",
                        "            spacing 5"
                    ] + ["            " + line for line in _mod_achievements.pop(0)]
                
                new_column_str.extend(new_achiev_str)
            
            new_screen_o.add(new_column_str)
        
        utils.create_screen("_ml_achievement_page_" + str(num), tag="achievement_page") \
            .extend(new_screen_o.build())
