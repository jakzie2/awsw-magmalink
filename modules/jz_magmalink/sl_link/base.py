from renpy.display import layout
from renpy.sl2 import slast 

import jz_magmalink.common as common
import jz_magmalink.sl_link.utils as utils

class SLSearchable:
    def _search(self, condition, name, needle):
        return utils._search(self.parent_node, self.index, condition, name, needle)
    
    def search_window(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable is layout.Window, "window", needle)
    
    def search_box(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable is layout.MultiBox, "hbox/vbox", needle)
    
    def search_textbutton(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable.__name__ == "_textbutton", "textbutton", needle)
    
    def search_imagebutton(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable.__name__ == "_imagebutton", "imagebutton", needle)
    
    def search_viewport(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable.__name__ == "sl2viewport", "viewport", needle)
    
    def search_add(self, needle=None):
        return self._search(lambda n: hasattr(n,'displayable') and n.displayable.__name__ == "sl2add", "add", needle)
    
    def search_if(self, needle=None):
        return self._search(lambda n: isinstance(n, slast.SLIf), "if", needle)
    
    def collect(self, next_func, limit=100):
        nodes = []
        self.for_each(next_func, lambda i, n: nodes.append(n))
        
        return nodes
    
    def for_each(self, next_func, func, limit=100):
        next_node = self
        for i in range(limit):
            try:
                next_node = next_func(next_node)
            except common.MagmaLinkError:
                break
            if next_node is None:
                break
            func(i, next_node)
        
        return self

class SLNode(SLSearchable):
    def __init__(self, parent_node, index):
        self.parent_node = parent_node
        self.index = index
        self.node = parent_node.children[index]
    
    def insert(self, displayable):
        if isinstance(displayable, SLBranch):
            displayable = displayable.parent_node
        if self.index == len(self.parent_node.children) - 1:
            self.parent_node.children.extend(displayable.children)
        else:
            for i, child in enumerate(displayable.children):
                self.parent_node.children.insert(self.index + i + 1, child)
        return self
    
    def replace_by(self, node):
        self.parent_node.children[self.index] = node.node
        self.node = node.node
        
        return self

class SLBlock(SLNode):
    def branch(self):
        return SLBranch(self.node)

class SLBranch(SLSearchable):
    def __init__(self, parent_node):
        self.parent_node = parent_node
    
    def _search(self, condition, name, needle):
        return utils._search(self.parent_node, 0, condition, name, needle, include_base=True)
    
    def first(self):
        return utils.slnode(self.parent_node, 0)
    
    def extend(self, nodes):
        if isinstance(nodes, (list, tuple)):
            for node in nodes:
                if isinstance(node, SLNode):
                    self.parent_node.children.append(node.node)
                elif isinstance(node, slast.SLNode):
                    self.parent_node.children.append(node)
                else:
                    common.error("Object of type " + type(node).__name__ + "is not SL node.")
        elif isinstance(nodes, SLBranch):
            self.parent_node.children.extend(nodes.parent_node.children)
        else:
            common.error("Invalid parameter for `extend` function.")
        
        return self
    
    def clear(self):
        self.parent_node.children[:] = []
        
        return self

class SLIf(SLNode):
    def branch(self, condition):
        for cond, block in self.node.entries:
            if cond == condition:
                return SLBranch(block)
        common.error("Condition %r not found in if statement." % condition)

    def branch_else(self):
        return self.branch(None)
