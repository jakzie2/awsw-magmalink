class MagmaLinkError(Exception):
    pass

def error(text):
    raise MagmaLinkError("\n    " + text.replace("\n", "\n    "))

_loading = True
