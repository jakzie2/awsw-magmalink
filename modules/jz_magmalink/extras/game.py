import renpy
import pygame
from renpy.display import im

import jz_magmalink.common as common
import jz_magmalink.ast_link.runtime as runtime

class Object:
    _count = 0
    
    def __init__(self, game, images, xpos, ypos, xvel, yvel, rotation, bbox, flags):
        Object._count += 1
        self.id = Object._count
        if isinstance(images, list):
            self.image = images[0]
            self.images = images
        else:
            self.image = images
            self.images = [images]
        self.xpos = xpos
        self.ypos = ypos
        self.rotation = rotation
        self.xvel = xvel
        self.yvel = yvel
        self.bbox = bbox
        self.flags = set(flags)
        
        self._game = game
        self._prev_rot = rotation
        self._cache_img = self.image
        self._cache_size = None
        self._cache_shift = (0, 0)
        self._cache_collisions = set()
        self._effects = {}
        
        game._images.update(self.images)
    
    def update(self):
        self.move(self.xvel, self.yvel)
    
    def update_effects(self):
        for name, eff in self._effects.items():
            if eff[0] is not None and self._game.st >= eff[0]:
                if eff[1] is not None:
                    eff[1](self)
                self._effects.pop(name)
    
    def move(self, xvel, yvel):
        self.xpos += self._game.dtime * xvel
        self.ypos += self._game.dtime * yvel
    
    def collides_with(self, obj):
        if isinstance(obj, (list, tuple, set)):
            for o in obj:
                if self.collides_with(o):
                    return True
            return False
        
        if self.bbox is None or obj.bbox is None:
            return False
        if self.xpos + self.bbox[0] + self.bbox[2] < obj.xpos + obj.bbox[0]:
            return False
        if obj.xpos + obj.bbox[0] + obj.bbox[2] < self.xpos + self.bbox[0]:
            return False
        if self.ypos + self.bbox[1] + self.bbox[3] < obj.ypos + obj.bbox[1]:
            return False
        if obj.ypos + obj.bbox[1] + obj.bbox[3] < self.ypos + self.bbox[1]:
            return False
            
        return True
    
    def collision_end_with(self, obj):
        col = self.collides_with(obj)
        col_prev = obj.id in self._cache_collisions
        if col and not col_prev:
            self._cache_collisions.add(obj.id)
        elif not col and col_prev:
            self._cache_collisions.remove(obj.id)
            
        return col_prev and not col
    
    def collides_with_screen_edge(self):
        if self.bbox is None:
            return False
        return self.xpos <= 0 or self.xpos + self.bbox[0] + self.bbox[2] >= 1920 or self.ypos <= 0 or self.ypos + self.bbox[1] + self.bbox[3] >= 1080
    
    def is_off_screen(self):
        size_x, size_y = self.get_size()
        return self.xpos < -size_x or self.xpos > 1920 or self.ypos < -size_y or self.ypos > 1080
        
    def wrap_around_screen(self):
        size_x, size_y = self.get_size()
        if self.xpos < -size_x:
            self.xpos = 1920
        elif self.xpos > 1920:
            self.xpos = -size_x
        if self.ypos < -size_y:
            self.ypos = 1080
        elif self.ypos > 1080:
            self.ypos = -size_y
        
    def switch_image(self, i):
        self.image = self.images[i]
        self._cache_img = None
        self._cache_size = None
        
    def add_effect(self, name, duration=None, func_end=None):
        self._effects[name] = (None if duration is None else (self._game.st + duration), func_end)
        
    def remove_effect(self, name, infinite=None):
        if self.has_effect(name, infinite):
            eff = self._effects[name]
            if eff[1] is not None:
                eff[1](self)
            self._effects.pop(name)
        
    def has_effect(self, name, infinite=None):
        has = name in self._effects
        if infinite is None or not has:
            return has
        return infinite == (self._effects[name][0] is None)
        
    def get_size(self):
        if self._cache_size is None:
            self._cache_size = im.cache.get(self.image).get_size()
        return self._cache_size
        
    def render(self):
        if self.rotation:
            if self.rotation > 180:
                self.rotation -= 360
            elif self.rotation < -180:
                self.rotation += 360
                
            if self._cache_img is None or self.rotation != self._prev_rot:
                self._cache_img = im.Rotozoom(self.image, self.rotation, 1.0)
                rect = im.cache.get(self.image).get_rect(center=(0,0))
                new_rect = im.cache.get(self._cache_img).get_rect(center=rect.center)
                self._cache_shift = (new_rect.x + rect.width/2, new_rect.y + rect.height/2)
        else:
            self._cache_shift = (0, 0)
            self._cache_img = self.image
        self._prev_rot = self.rotation
        
        return self._game.render_image(self._cache_img, self.xpos + self._cache_shift[0], self.ypos + self._cache_shift[1])
    
    def show(self):
        renpy.exports.show("_ml_object_" + str(self.id), at_list=[runtime.context["Position"](xpos=self.xpos, ypos=self.ypos, xanchor=0, yanchor=0)], what=self.image)
    
    def hide(self):
        renpy.exports.hide("_ml_object_" + str(self.id))


class GameDisplayable(renpy.display.core.Displayable):
    def __init__(self, game_class, store):
        renpy.display.core.Displayable.__init__(self)
        
        self.store = store
        
        self._images = set()
        self._keys = {}
        self._tasks = []
        
        self._r = None
        self._oldst = None
        self.st = 0
        self.at = 0
        self.dtime = 0
        
        self._ending = False
        self._return_value = None
        
        self._game = game_class(self)
    
    def visit(self):
        return list(self._images)
    
    def render(self, width, height, st, at):
        self.st = st
        self.at = at
        self._r = renpy.display.render.Render(width, height)
        
        if self._oldst is None:
            self._oldst = st
                
        self.dtime = self.st - self._oldst
        self._oldst = self.st
        
        for i, task in enumerate(self._tasks):
            if st >= task[0]:
                task[1]()
                self._tasks.pop(i)
        
        self._game.process()
        
        renpy.display.render.redraw(self, 0)
        return self._r
    
    def event(self, ev, x, y, st):
        if self._ending:
            return self._return_value
        
        if ev.type == pygame.KEYDOWN:
            if ev.key in self._keys:
                self._keys[ev.key] = True
        elif ev.type == pygame.KEYUP:
            if ev.key in self._keys:
                self._keys[ev.key] = False
        
        self.st = st
        self._game.on_event(ev, x, y)
        
        raise renpy.display.core.IgnoreEvent()
    
    def create_object(self, images, xpos=0, ypos=0, xvel=0, yvel=0, rotation=0, bbox=None, flags=[]):
        return Object(self, images, xpos, ypos, xvel, yvel, rotation, bbox, flags)
    
    def register_keys(self, keys):
        for key in keys:
            if key not in self._keys:
                self._keys[key] = False
    
    def schedule_task(self, time, func):
        self._tasks.append((self.st + time, func))
    
    def is_key_down(self, key):
        if key not in self._keys:
            common.error("Key " + str(key) + " is not registered.")
        return self._keys[key]
    
    def render_image(self, image, xpos, ypos):
        obj = renpy.display.render.render(image, 1920, 1080, self.st, self.at)
        self._r.blit(obj, (xpos, ypos))
        return obj
    
    def end(self, return_value=False):
        if not self._ending:
            self._ending = True
            self._return_value = return_value
            self._tasks[:] = []
            mouse_x, mouse_y = renpy.display.draw.get_mouse_pos()
            renpy.game.interface.set_mouse_pos(mouse_x + 10, mouse_y, None)

class Game(object):
    def __init__(self, game):
        super(Game, self).__init__()
        self.game = game
    
    def process(self):
        pass
    
    def on_event(self, ev, x, y):
        pass

def init_game(game_class, store=None):
    return GameDisplayable(game_class, store)._game

def play_game(game_class, store=None, suppress_overlay=False, suppress_underlay=False):
    renpy.ui.add(game_class.game if isinstance(game_class, Game) else GameDisplayable(game_class, store))
    
    return renpy.ui.interact(suppress_overlay=suppress_overlay, suppress_underlay=suppress_underlay)

def bind_game_store(store, game_class):
    store["game"] = None
    store["result"] = None
    
    @store.export("init")
    def init():
        store["game"] = init_game(game_class, store)
        
    @store.export("play")
    def play(suppress_overlay=False, suppress_underlay=False):
        store["result"] = play_game(store["game"] or game_class, store, suppress_overlay, suppress_underlay)
